const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const app = express()
const port = 3000;

const uri = 'mongodb+srv://books_admin:books_adm_tmpn@cluster0-rsalq.mongodb.net/test?retryWrites=true'
const localUri = 'mongodb://localhost/books'

mongoose.connect(localUri, (err) =>{
  if(err){console.log('MONGOOSE NOT CONNECT: ', err)}
  else{console.log('MONGOOSE CONNECTED')}
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))

//routes
const books = require('./routes/books')

app.use('/books', books)

app.listen(process.env.PORT || port, (err)=> {
  if(err){console.log('ERROR NODEJS: ', err)}
  else{ console.log('NODEJS RUNNING ON PORT: ', port)}
})

module.exports = app
