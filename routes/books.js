const routes = require('express').Router()
const booksController = require('../controllers/books')

routes.get('/', booksController.findAll)
routes.post('/', booksController.create)

module.exports = routes