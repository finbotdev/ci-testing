const BooksModel = require('../models/Books.js')


class BooksController {
  constructor() {

  }

  static create(req,res,next){
    
    BooksModel.create({
      title: req.body.title,
      author: req.body.author,
      price: req.body.price
    }).then( result => {
      res.status(201).send(result)
    }).catch( err => {
      res.status(500).send( err)
    })
  }

  static findAll(req,res,next){
    BooksModel.find()
    .then( result => {
      res.status(200).send(result)
    }).catch( err => {
      console.log('error get all: ', err)
      res.status(500).send(err)
    })
  }

  static findOne(req,res,next){

  }

  static remove(req,res,next){

  }
}

module.exports = BooksController