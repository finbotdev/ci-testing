const mongoose = require('mongoose')
const Schema = mongoose.Schema;

var booksSchema = new Schema({
  title:  String,
  author: String,
  price: Number
}, {timestamps:true})


var booksModel = mongoose.model('books', booksSchema)

module.exports = booksModel